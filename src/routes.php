<?php

//CategoryController routes
Route::get('article/category/{full_permalink}/delete', 'WorkInProgress\Blog\CategoryController@delete')->where('full_permalink', '(.*)');
Route::get('article/category/{full_permalink}/edit', 'WorkInProgress\Blog\CategoryController@edit')->where('full_permalink', '(.*)');
Route::put('article/category/{full_permalink}/order', 'WorkInProgress\Blog\CategoryController@order')->where('full_permalink', '(.*)');
Route::put('article/category/{full_permalink}', 'WorkInProgress\Blog\CategoryController@update')->where('full_permalink', '(.*)');
Route::resource('article/category', 'WorkInProgress\Blog\CategoryController');

//ArticleImageController routes
Route::get('article/{full_permalink}/images', 'WorkInProgress\Blog\ArticleImageController@index')->where('full_permalink', '(.*)');
Route::post('article/{full_permalink}/image', 'WorkInProgress\Blog\ArticleImageController@store')->where('full_permalink', '(.*)');
Route::put('article/{full_permalink}/image/{id}', 'WorkInProgress\Blog\ArticleImageController@update')->where('full_permalink', '(.*)');
Route::delete('article/{full_permalink}/image/{id}', 'WorkInProgress\Blog\ArticleImageController@delete')->where('full_permalink', '(.*)');

//ArticleController routes
Route::get('articles', 'WorkInProgress\Blog\ArticleController@index');
Route::get('article/{full_permalink}/delete', 'WorkInProgress\Blog\ArticleController@delete')->where('full_permalink', '(.*)');
Route::get('article/{full_permalink}/edit', 'WorkInProgress\Blog\ArticleController@edit')->where('full_permalink', '(.*)');
Route::put('article/{full_permalink}/order', 'WorkInProgress\Blog\ArticleController@order')->where('full_permalink', '(.*)');
Route::put('article/{full_permalink}', 'WorkInProgress\Blog\ArticleController@update')->where('full_permalink', '(.*)');
Route::resource('article', 'WorkInProgress\Blog\ArticleController');

?>
