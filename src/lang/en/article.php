<?php

return [
  'permalink' => 'This is the URL the page will display on.',
  'product_category_id' => 'Category in which this page will appear under in navigation menus.',
  'price' => 'Price of item including GST.',
  'tags' => 'Identify your products easier by tagging them.',
  'related_products' => 'Items that can be cross sold with this item.',
  'short_description' => 'Brief overview of the page diplayed in the site map or in search results.',
  'attributes' => 'Active products are accessible by the public.'
];
