<?php

return [
  'permalink' => 'This is the URL the page will display on.',
  'product_category_id' => 'Category in which this page will appear under in navigation menus.',
  'short_description' => 'Brief overview of the category diplayed in the site map or in search results.',
  'attributes' => 'Active pages are accessible by the public.',
];
