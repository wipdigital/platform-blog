<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTitleAndUrlColumnsToBlogArticleImageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('blog_article_images', function(Blueprint $table)
		{
      $table->text('title');
      $table->text('url');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('blog_article_images', function(Blueprint $table)
		{
      $table->dropColumn('title');
      $table->dropColumn('url');
		});
	}

}
