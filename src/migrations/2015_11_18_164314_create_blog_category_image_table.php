<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogCategoryImageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blog_category_images', function(Blueprint $table)
		{
			$table->increments('id');
      $table->integer('blog_category_id')->unsigned();
      $table->foreign('blog_category_id')->references('id')->on('blog_categories');
      $table->string('src');
      $table->text('short_description')->nullable();
      $table->boolean('featured')->default(false);
      $table->boolean('active')->default(true);
      $table->integer('order');
			$table->timestamps();
      $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blog_category_images');
	}

}
