<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blog_categories', function(Blueprint $table)
		{
			$table->increments('id');
      $table->integer('blog_category_id')->unsigned()->nullable()->default(1);
      $table->foreign('blog_category_id')->references('id')->on('blog_categories');
      $table->string('title');
      $table->string('seo_title');
      $table->string('permalink');
      $table->string('full_permalink');
      $table->text('short_description');
      $table->text('description');
      $table->boolean('active')->default(true);
      $table->integer('order');
			$table->timestamps();
      $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blog_categories');
	}

}
