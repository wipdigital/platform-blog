<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBlogArticleImageTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('blog_article_images', function(Blueprint $table)
		{
			$table->increments('id');
      $table->integer('blog_article_id')->unsigned();
      $table->foreign('blog_article_id')->references('id')->on('blog_articles');
      $table->string('src');
      $table->text('short_description');
      $table->boolean('featured')->default(false);
      $table->boolean('active')->default(true);
      $table->integer('order');
			$table->timestamps();
      $table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('blog_article_images');
	}

}
