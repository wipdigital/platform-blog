<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTemplateIdToBlogCategoryTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('blog_categories', function(Blueprint $table)
		{
      $table->integer('template_id')->unsigned()->nullable()->default(1);
      $table->foreign('template_id')->references('id')->on('templates');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('blog_categories', function(Blueprint $table)
		{
			$table->dropColumn('template_id');
		});
	}

}
