<?php namespace WorkInProgress\Blog;

class CategoryTableSeeder extends \Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
    \DB::table('blog_categories')->delete();

    Category::create(array(
      'blog_category_id' => null,
      'title' => 'Articles',
      'permalink' => 'articles',
      'full_permalink' => 'articles',
      'description' => ''
    ));

	}

}
