@extends('layouts.standard')

@section('main')
  {{ Form::model($article, ['route' => array('article.update', $article->full_permalink), 'method' => 'PUT', 'data-abide']) }}
    <aside class="right fxd-mobile">
      <a href="/article/{{ $article->full_permalink }}/images" title="Images" class="button tiny radius"><i class="fa fa-files-o"></i> Images</a>
    </aside>

    <h1>Edit Article</h1>
    @include('blog::article.form')

    <div class="row max-width collapse">
      <div class="small-12 save-col columns">
        {{ Form::submit('Save Article', array('class' => 'radius right button')) }}
      </div>
    </div>

  {{ Form::close() }}
@stop
