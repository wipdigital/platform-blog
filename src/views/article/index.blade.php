@extends('layouts.standard')


@section('main')
  <aside class="right fxd-mobile show-for-large-up">
    <a href="/article/create" title="Create" class="button tiny radius"><i class="fa fa-tag"></i> Create Article</a>
    <a href="/article/category/create" title="Create" class="button tiny radius"><i class="fa fa-folder-open"></i> Create Category</a>
  </aside>
  <aside class="right fxd-mobile show-for-small-up hide-for-large-up">
    <button href="#" data-dropdown="drop1" aria-controls="drop1" aria-expanded="false" class="button radius tiny dropdown">Create</button><br>
    <ul id="drop1" data-dropdown-content class="f-dropdown" aria-hidden="true">
      <li><a href="/article/create"><i class="fa fa-tag"></i> New Article</a></li>
      <li><a href="/article/category/create"><i class="fa fa-folder-open"></i> New Category</a></li>
    </ul>
  </aside>

  <h1>articles</h1>

  @if(Session::has('message'))
  <div data-alert class="alert-box success">
    {{ Session::get('message') }}
    <a href="#" class="close">&times;</a>
  </div>
  @endif

  <div class="hierarchy">
    @if($hierarchy)
      {{ $hierarchy }}
    @else
    <div class="small-12 columns">
      <em>No articles to display.</em>
    </div>
    @endif
  </div>

  @include('components.confirmation')
@stop
