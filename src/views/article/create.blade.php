@extends('layouts.standard')

@section('main')
  <h1>Create Article</h1>

  {{ Form::open(['url' => 'article', 'data-abide']) }}
    @include('blog::article.form')

    <div class="row max-width collapse">
      <div class="small-12 save-col columns">
        <div class="right">
          {{ Form::submit('Create Article', ['class' => 'radius button']) }}
        </div>
      </div>
    </div>

  {{ Form::close() }}
@stop
