@extends('layouts.standard')

@section('main')
  <h1>Edit Category</h1>

  {{ Form::model($category, ['route' => array('article.category.update', $category->full_permalink), 'method' => 'PUT', 'data-abide']) }}
    @include('blog::category.form')
  {{ Form::close() }}
@stop
