@if($errors->first())
  <div class="alert-box">
    <h2>Error</h2>

    @foreach($errors->all() as $error)
    <p>{{ $error }}</p>
    @endforeach
  </div>
@endif

<div class="row max-width collapse">
  <div class="medium-6 columns">
    <div class="image-well panel radius">
      <a href="#" class="drop-area radius square @if($featured_responsive_filemanager_url) active @endif" data-reveal-id="featured-responsive-filemanager" @if($featured_responsive_filemanager_url) style="background-size: cover; background-image: url('{{ Config::get('blog::article.cdn') . $featured_responsive_filemanager_url . Config::get('blog::article.query') }}');" @endif>
        <div class="interaction valign-cent cent-align">
          <!--<h4>Drag photos here to upload</h4>-->
          {{ Form::hidden('featured_responsive_filemanager_url', $featured_responsive_filemanager_url, array('id' => 'featured-responsive-filemanager-url', 'class' => 'responsive-filemanager-url')) }}
          <button class="button tiny browse" data-reveal-id="featured-responsive-filemanager"><i class="fa fa-camera"></i><br>Browse/Upload Image</button>
        </div>
      </a>
    </div>
  </div>
</div>

<div class="title-field @if($errors->has('title'))error @endif">
  {{ Form::label('title', 'Title') }}
  {{ Form::text('title', null, ['class' => 'radius', 'required']) }}
  @if($errors->has('title'))
  <small class="error">{{ $errors->first('title') }}</small>
  @endif
  <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Title']) }}</small>
</div>

@if(!(isset($category) && $category->permalink == 'articles'))
  <div class="blog-category-id-field @if($errors->has('blog_category_id'))error @endif">
    <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ \Lang::get('blog::category.blog_category_id') }}"><i class="fa fa-question-circle"></i></span>
    {{ Form::label('blog_category_id', 'Parent Category') }}
    <select name="blog_category_id" required>
      {{ $options }}
    </select>
    @if($errors->has('blog_category_id'))
    <small class="error">{{ $errors->first('blog_category_id') }}</small>
    @else
    <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Parent Category']) }}</small>
    @endif
  </div>
@endif

<div class="short-description-field @if($errors->has('short_description'))error @endif">
  <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ \Lang::get('blog::category.short_description') }}"><i class="fa fa-question-circle"></i></span>
  {{ Form::label('short_description', 'Short Description') }}
  {{ Form::text('short_description', null, ['class' => 'radius']) }}
  @if($errors->has('short_description'))
  <small class="error">{{ $errors->first('short_description') }}</small>
  @else
  <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Short Description']) }}</small>
  @endif
</div>

<div class="content-editor"> 
  {{ Form::label('description', 'Content') }}</span>
  {{ Form::textarea('description', null, ['class' => 'froala-textarea']) }}
</div>

<dl class="accordion" data-accordion>
  <dd class="accordion-navigation @if($errors->has('seo_title') or $errors->has('permalink')) active @endif">
    <a href="#panel1b">Advanced Settings <i class="fa fa-caret-down"></i></a>
    <div id="panel1b" class="content @if($errors->has('seo_title') or $errors->has('permalink')) active @endif">
      <div class="seo-title-field @if($errors->has('seo_title'))error @endif">
        {{ Form::label('seo_title', 'SEO Title') }}
        {{ Form::text('seo_title', null, ['class' => 'radius']) }}
        @if($errors->has('seo_title'))
        <small class="error">{{ $errors->first('seo_title') }}</small>
        @else
        <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'SEO Title']) }}</small>
        @endif
      </div>

      @if(!(isset($category) && $category->permalink == 'articles'))
        <div class="permalink-field @if($errors->has('permalink'))error @endif">
          <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ \Lang::get('blog::category.permalink') }}"></i></span>
          {{ Form::label('permalink', 'Permalink') }}
          {{ Form::text('permalink', null, ['class' => 'radius', 'required']) }}
          @if($errors->has('permalink'))
          <small class="error">{{ $errors->first('permalink') }}</small>
          @else
          <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Permalink']) }}</small>
          @endif
        </div>
        <div class="template-id-field @if($errors->has('template_id'))error @endif">
          <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ \Lang::get('blog::category.template_id') }}"><i class="fa fa-question-circle"></i></span>
          {{ Form::label('template_id', 'Template') }}
          {{ Form::select('template_id', $template_options, null, ['required']) }}
          @if($errors->has('template_id'))
          <small class="error">{{ $errors->first('template_id') }}</small>
          @else
          <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Template']) }}</small>
          @endif
        </div>
      @endif
    </div>
  </dd>
</dl>

<div class="row max-width collapse switch-label">
  <div class="small-12 medium-6 columns">
    <h3 class="states">ATTRIBUTES
      <span data-tooltip aria-haspopup="true" class="has-tip" title="{{ \Lang::get('blog::category.attributes') }}"><i class="fa fa-question-circle"></i></span>
    </h3>
    {{ Form::label('active', '', ['class' => 'inline left']) }}
    <div class="switch tiny left">
      {{ Form::checkbox('active', true, false) }}
      <label for="active"></label>
    </div> 
  </div>
</div>

<div class="row max-width collapse">
  <div class="small-12 save-col columns">
    {{ Form::submit('Save Category', array('class' => 'radius right button')) }}
  </div>
</div>

{{ Form::close() }}

<input type="hidden" id="froala-responsive-filemanager-url">
<div id="froala-responsive-filemanager" class="reveal-modal" data-reveal aria-labelledby="File Manager" aria-hidden="true" role="dialog">
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
  <iframe src="/responsive_file_manager/filemanager/dialog.php?type=1&field_id=froala-responsive-filemanager-url"></iframe>
</div>

<div id="featured-responsive-filemanager" class="reveal-modal" data-reveal aria-labelledby="File Manager" aria-hidden="true" role="dialog">
  <a class="close-reveal-modal" aria-label="Close">&#215;</a>
  <iframe src="/responsive_file_manager/filemanager/dialog.php?type=1&field_id=featured-responsive-filemanager-url"></iframe>
</div>

@section('inline_js')
<script>
(function(jQuery) {

  var convertToSlug = function(str) {
    return str
          .toLowerCase()
          .replace(/[^\w ]+/g,'')
          .replace(/ +/g,'-');
  } 

  $('#title').on('input', function() {
    $('#permalink').val(convertToSlug($(this).val()));
  });
})(jQuery);
</script>
@stop
