@extends('layouts.standard')

@section('main')
  <h1>Create Category</h1>

  {{ Form::open(['url' => 'article/category', 'data-abide']) }}
    @include('blog::category.form')
  {{ Form::close() }}
@stop
