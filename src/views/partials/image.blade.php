<div class="row max-width separator">
  <div class="cent-align valign-cent loading"><i class="fa fa-cog fa-spin"></i></div>
  {{ Form::model($image, ['url' => '/article/' . $article->full_permalink . '/image/' . $image->id, 'method' => 'PUT', 'class' => 'image', 'data-abide']) }}

    <div class="small-12 columns">
      <div class="row">
        <div class="small-12 medium-6 large-4 columns">
          <div class="image-well panel radius">
            <a href="#" class="drop-area radius square" data-reveal-id="image-add-modal-{{ $image->id }}" style="background-image: url('{{ $image->src }}');">
              <div class="interaction valign-cent cent-align">
                {{ Form::hidden('src', $image->src, ['id' => 'image-add-' . $image->id]) }}
                <button class="button tiny">Browse</button>
              </div>
            </a>
          </div>
        </div>

        <div class="small-12 medium-6 large-8 columns">
          <div class="title-field @if($errors->has('title'))error @endif">
            {{ Form::label('title', 'Title') }}
            {{ Form::text('title', null, ['class' => 'radius', 'required']) }}
            <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Title']) }}</small>
          </div>

          <div class="short-description-field @if($errors->has('short_description'))error @endif">
            {{ Form::label('short_description', 'Short Description') }}
            {{ Form::text('short_description', null, ['class' => 'radius']) }}
            <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'Short Description']) }}</small>
          </div>

          <div class="url-field @if($errors->has('url'))error @endif">
            {{ Form::label('url', 'URL') }}
            {{ Form::text('url', null, ['class' => 'radius']) }}
            <small class="error">{{ \Lang::get('validation.required', ['attribute' => 'URL']) }}</small>
          </div>

          {{ Form::submit('Save', ['class' => 'button tiny success']) }}
          <a class="button tiny alert delete" href="/article/{{ $article->full_permalink }}/image/{{ $image->id }}"><i class="fa fa-trash"></i> Delete</a>

          <div id="image-add-modal-{{ $image->id }}" class="reveal-modal" data-reveal aria-labelledby="File Manager" aria-hidden="true" role="dialog">
            <a class="close-reveal-modal" aria-label="Close">&#215;</a>
            <iframe src="/responsive_file_manager/filemanager/dialog.php?type=1&field_id=image-add-{{ $image->id }}"></iframe>
          </div>
        </div>
      </div>
    </div>
  {{ Form::close() }}
</div>
