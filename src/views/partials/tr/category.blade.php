<div class="hammer row max-width">
  <div class="small-12 medium-7 columns">
    @for ($i=0; $i<$depth; $i++) <span class="indent"></span> @endfor <i class="fa fa-fw fa-folder-open overlap"></i> @if($category->images()->first())<img src="{{ Config::get('blog::article.cdn') . $category->images()->first()->src . Config::get('blog::article.thumbnail_query') }}" class="product-thumbnail hidden-for-small"> @endif <a href="/article/category/{{ $category->full_permalink }}/edit" title="Edit {{ $category->title }}">{{ $category->title }}</a> @if(!$category->active) <small>(DISABLED)</small> @endif
  </div>
  <aside class="small-12 medium-5 columns table-right">
    {{ Form::open(array('action' => array('WorkInProgress\Blog\CategoryController@order', $category->full_permalink), 'method' => 'PUT', 'class' => 'order')) }}
      {{ Form::selectRange('order', 1, $max_order, $category->order) }}
    {{ Form::close() }}

    <a href="/article/category/{{ $category->full_permalink }}/edit" title="Edit" class="edit button tiny secondary"><i class="fa fa-pencil"></i><span class="show-for-large-up">Edit</span></a>
    <a href="/article/category/{{ $category->full_permalink }}/delete" title="Delete" class="delete button tiny alert" data-entry="category"><i class="fa fa-trash"></i><span class="show-for-large-up">Delete</span></a>
  </aside>
</div>
