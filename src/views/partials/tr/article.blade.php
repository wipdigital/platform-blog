<div class="hammer row max-width">
  <div class="small-12 medium-7 columns">
    @for ($i=0; $i<$depth; $i++) <span class="indent"></span> @endfor <i class="fa fa-fw fa-tag overlap"></i> <a href="/article/{{ $article->full_permalink }}/edit" title="Edit"> {{ $article->title }}</a> @if(!$article->active) <small>(DISABLED)</small> @endif
  </div>
  <aside class="small-12 medium-5 columns table-right">
    {{ Form::open(array('action' => array('WorkInProgress\Blog\ArticleController@order', $article->full_permalink), 'method' => 'PUT', 'class' => 'order')) }}
      {{ Form::selectRange('order', 1, $max_order, $article->order) }}
    {{ Form::close() }}

    <a href="/article/{{ $article->full_permalink }}/edit" title="Edit" class="edit button tiny secondary"><i class="fa fa-pencil"></i><span class="show-for-large-up">Edit</span></a>
    <a href="/article/{{ $article->full_permalink }}/delete" title="Delete" class="delete button tiny alert" data-entry="article"><i class="fa fa-trash"></i><span class="show-for-large-up">Delete</span></a>
  </aside>
</div>
