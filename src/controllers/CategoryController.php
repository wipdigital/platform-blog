<?php namespace WorkInProgress\Blog;

class CategoryController extends \BaseController {

	private $rules = [
    'title' => 'required'
  ];

  private $image_labels = ['featured_responsive_filemanager_url'];

  public function __construct()
  {
    $this->beforeFilter('auth');
  }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		return \Redirect::to('articles');
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
  {
    $template_options = Template::where('type', '=', '3')->get()->lists('name', 'id');

    $data = [
      'options' => \App::make('WorkInProgress\Blog\ArticleController')->getHierarchy(0, 0, 'blog::partials.option', \Input::old('blog_category_id') ?: null),
      'template_options' => $template_options
    ];

    foreach($this->image_labels as $key => $image_label) {
      $data[$image_label] = null;
    }

    return \View::make('blog::category.create', $data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
    $data = \Input::all();
    $data['active'] = (\Input::has('active')) ? true : false;

    $rules = $this->rules;
    $rules['permalink'] = 'required|unique:blog_categories,permalink,NULL,id,blog_category_id,' . $data['blog_category_id'] . ',deleted_at,NULL|unique:blog_articles,permalink,NULL,id,blog_category_id, ' . $data['blog_category_id'] . ',deleted_at,NULL';
    $validator = \Validator::make($data, $rules);

    if($validator->fails()) {
      return \Redirect::route('article.category.create')->withErrors($validator)->withInput();
    }

    if($category = Category::create($data)) {
      $category->save();

      //Save the images
      foreach($this->image_labels as $key => $image_label) {
        if(\Input::has($image_label)) {
          $image = new CategoryImage(array(
            'order' => $key + 1,
            'src' => \Input::get($image_label),
            'featured' => ($image_label == 'featured_responsive_filemanager_url') ? true : false
          ));
          
          $category->images()->save($image);
        }
      }

      \Session::flash('message', 'Category successfully created!');
      return \Redirect::to('articles');
    }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return \Redirect::to('articles');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  string $permalink
	 * @return Response
	 */
	public function edit($full_permalink)
  {
    $category = Category::where('full_permalink', '=', $full_permalink)->firstOrFail();

    $template_options = Template::where('type', '=', '3')->get()->lists('name', 'id');

    $data = [
      'category' => $category,
      'options' => \App::make('WorkInProgress\Blog\ArticleController')->getHierarchy(0, 0, 'blog::partials.option', \Input::old('blog_category_id') ?: $category->blog_category_id),
      'template_options' => $template_options
    ];

    //Grab the image URLs
    foreach($this->image_labels as $key => $image_label) {
      $image = $category->images()->where('order', '=', $key+1)->first();

      $data[$image_label] = $image ? $image->src : null;
    }

    return \View::make('blog::category.edit', $data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($full_permalink)
	{
    $category = Category::where('full_permalink', '=', $full_permalink)->firstOrFail();

    $data = \Input::all();
    $data['active'] = (\Input::has('active')) ? true : false;

    $rules = $this->rules;
    if($category->permalink == 'articles') {
      unset($rules['permalink']);
    } else {
      $rules['blog_category_id'] = 'required|not_in:' . $category->id;
      $rules['permalink'] = 'required|unique:blog_categories,permalink,' . $category->id . ',id,blog_category_id,' . $data['blog_category_id'] . ',deleted_at,NULL|unique:blog_articles,permalink,' . $category->id . ',id,blog_category_id, ' . $data['blog_category_id'] . ',deleted_at,NULL';
    }

    $validator = \Validator::make($data, $rules);

    if($validator->fails()) {
      return \Redirect::route('article.category.edit', $full_permalink)->withErrors($validator)->withInput();
    }

    $category_id = $category->blog_category_id;
    if($category->update($data)) {
      //if the category has moved
      if(isset($data['blog_category_id']) && $category_id != $data['blog_category_id']) {
        $category->order = Category::where('blog_category_id', '=', $category->blog_category_id)->count();
        $category->save();

        $categories = Category::where('blog_category_id', '=', $category->blog_category_id)->orderBy('order', 'asc')->get();

        $i = 1;
        foreach($categories as $category) {
          $category->order = $i;
          $i++;
          $category->save();
        }

        $articles = Article::where('blog_category_id', '=', $category->blog_category_id)->orderBy('order', 'asc')->get();

        $i = 1;
        foreach($articles as $article) {
          $article->order = $i;
          $i++;
          $article->save();
        }
      }

      //Update images
      foreach($this->image_labels as $key => $image_label) {
        if(\Input::has($image_label)) {
          $data = array(
            'order' => $key + 1,
            'src' => \Input::get($image_label),
            'featured' => ($image_label == 'featured_responsive_filemanager_url') ? true : false
          );

          $image = $category->images()->where('order', '=', $key+1)->first();
          if($image) {
            $image->update($data);
          } else {
            $image = new CategoryImage($data);
            $category->images()->save($image);
          }
        }
      }

      \Session::flash('message', 'Category successfully updated!');
      return \Redirect::to('articles');
    }
	}

  public function delete($full_permalink) {
    $category = Category::where('full_permalink', '=', $full_permalink)->firstOrFail();

    if($category->delete()) {
      \Session::flash('message', 'Category successfully deleted!');
      return \Redirect::to('articles');
    }
  }

  public function order($full_permalink)
  {
    $category = Category::where('full_permalink', '=', $full_permalink)->firstOrFail();
    $order = $category->order;
    $category->order = \Input::get('order');
    $category->save();

    if($order > $category->order) {
      $categories = Category::where('blog_category_id', '=', $category->blog_category_id)->orderBy('order', 'asc')->orderBy('updated_at','desc')->get();
    } else {
      $categories = Category::where('blog_category_id', '=', $category->blog_category_id)->orderBy('order', 'asc')->orderBy('updated_at','asc')->get();
    }

    $i = 1;
    foreach($categories as $category) {
      $category->order = $i;
      $i++;
      $category->save();
    }

    \Session::flash('message', 'Category order successfully updated!');
    return \Redirect::to('articles');
  }

}
