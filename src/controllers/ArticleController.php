<?php namespace WorkInProgress\Blog;

class ArticleController extends \BaseController {

	private $rules = [
    'title' => 'required'
  ];

  public function getHierarchy($blog_category_id = 0, $depth = 0, $template = 'blog::partials.tr.category', $selected = 1)
  {
    $hierarchy = '';

    if($blog_category_id) {
      $categories = Category::where('id', '=', $blog_category_id)->orderBy('order', 'asc')->get();
    } else {
      $categories = Category::whereNull('blog_category_id')->orderBy('order', 'asc')->get();
    }

    foreach($categories as $category) {

      $data = array(
        'depth' => $depth,
        'category' => $category,
        'selected' => $selected,
        'max_order' => Category::where('blog_category_id', '=', $category->blog_category_id)->count()
      );

      $hierarchy .= \View::make($template, $data);

      if($template == 'blog::partials.tr.category') {

        foreach($category->articles()->orderBy('order', 'asc')->get() as $article) {
          $data = array(
            'depth' => $depth + 1,
            'article' => $article,
            'max_order' => Article::where('blog_category_id', '=', $article->blog_category_id)->count()
          );

          $hierarchy .= \View::make('blog::partials.tr.article', $data);
        }
      }

      foreach($category->categories()->orderBy('order', 'asc')->get() as $category) {
        $hierarchy .= $this->getHierarchy($category->id, $depth+1, $template, $selected);
      }
    }

    return $hierarchy;
  }

  public function __construct()
  {
    $this->beforeFilter('auth');
    $this->beforeFilter('csrf', ['on' => 'post']);
  }

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
    $data = [
      'hierarchy' => $this->getHierarchy()
    ];

    return \View::make('blog::article.index', $data);
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
  {
    $template_options = Template::where('type', '=', '3')->get()->lists('name', 'id');

    $data = [
      'options' => $this->getHierarchy(0, 0, 'blog::partials.option', \Input::old('blog_category_id') ?: null),
      'template_options' => $template_options
    ];

    return \View::make('blog::article.create', $data);
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
    $data = \Input::all();
    $data['active'] = (\Input::has('active')) ? true : false;
    $data['premium'] = (\Input::has('premium')) ? true : false;
    $data['featured'] = (\Input::has('featured')) ? true : false;

    $rules = $this->rules;
    $rules['permalink'] = 'required|unique:blog_categories,permalink,NULL,id,blog_category_id,' . $data['blog_category_id'] . '|unique:blog_articles,permalink,NULL,id,blog_category_id, ' . $data['blog_category_id'] . ',deleted_at,NULL';
    $validator = \Validator::make($data, $rules);

    if($validator->fails()) {
      return \Redirect::route('article.create')->withErrors($validator)->withInput();
    }

    if($article = Article::create($data)) {
      if($data['featured']) {
        \DB::table('blog_articles')->where('featured', '=', 1)->where('id', '!=', $article->id)->update(['featured' => 0]);
      }

      \Session::flash('message', 'Article successfully created!');
      return \Redirect::to('articles');
    }
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		return \Redirect::to('articles');
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  string $permalink
	 * @return Response
	 */
	public function edit($full_permalink)
  {
    $article = Article::where('full_permalink', '=', $full_permalink)->firstOrFail();

    $template_options = Template::where('type', '=', '3')->get()->lists('name', 'id');

    $data = [
      'article' => $article,
      'options' => $this->getHierarchy(0, 0, 'blog::partials.option', \Input::old('blog_category_id') ?: $article->blog_category_id),
      'template_options' => $template_options
    ];

    return \View::make('blog::article.edit', $data);
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($full_permalink)
	{
    $article = Article::where('full_permalink', '=', $full_permalink)->firstOrFail();

    $data = \Input::all();
    $data['active'] = (\Input::has('active')) ? true : false;
    $data['premium'] = (\Input::has('premium')) ? true : false;
    $data['featured'] = (\Input::has('featured')) ? true : false;

    $rules = $this->rules;
    $rules['permalink'] = 'required|unique:blog_categories,permalink,' . $article->id . ',id,blog_category_id,' . $data['blog_category_id'] . '|unique:blog_articles,permalink,' . $article->id . ',id,blog_category_id, ' . $data['blog_category_id'] . ',deleted_at,NULL';

    $validator = \Validator::make($data, $rules);

    if($validator->fails()) {
      $data = [ 
        'options' => $this->getHierarchy(0, 0, 'blog::partials.option', $data['blog_category_id']),
        ];

      return \Redirect::route('article.edit', $full_permalink)->withErrors($validator)->withInput()->with($data);
    }

    $category_id = $article->blog_category_id;
    if($article->update($data)) {
      if($data['featured']) {
        \DB::table('blog_articles')->where('featured', '=', 1)->where('id', '!=', $article->id)->update(['featured' => 0]);
      }

      //if the article has moved
      if(isset($data['blog_category_id']) && $category_id != $data['blog_category_id']) {
        $article->order = Article::where('blog_category_id', '=', $article->blog_category_id)->count();
        $article->save();

        $articles = Article::where('blog_category_id', '=', $category_id)->orderBy('order', 'asc')->get();
        $i = 1;
        foreach($articles as $article) {
          $article->order = $i;
          $i++;
          $article->save();
        }
      }

      \Session::flash('message', 'Article successfully updated!');
      return \Redirect::to('articles');
    }
	}

  public function delete($full_permalink) {
    $article = Article::where('full_permalink', '=', $full_permalink)->firstOrFail();

    if($article->delete()) {
      \Session::flash('message', 'Article successfully deleted!');
      return \Redirect::to('articles');
    }
  }

  public function order($full_permalink)
  {
    $article = Article::where('full_permalink', '=', $full_permalink)->firstOrFail();
    $order = $article->order;
    $article->order = \Input::get('order');
    $article->save();

    if($order > $article->order) {
      $articles = Article::where('blog_category_id', '=', $article->blog_category_id)->orderBy('order', 'asc')->orderBy('updated_at','desc')->get();
    } else {
      $articles = Article::where('blog_category_id', '=', $article->blog_category_id)->orderBy('order', 'asc')->orderBy('updated_at','asc')->get();
    }

    $i = 1;
    foreach($articles as $article) {
      $article->order = $i;
      $i++;
      $article->save();
    }

    \Session::flash('message', 'Articles order successfully updated!');
    return \Redirect::to('articles');
  }

}
