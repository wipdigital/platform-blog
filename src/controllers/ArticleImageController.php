<?php namespace WorkInProgress\Blog;

class ArticleImageController extends \BaseController {

	private $rules = array(
    'blog_article_id' => 'required',
    'src' => 'required',
  );

  public function __construct()
  {
    $this->beforeFilter('auth');
  }

  public function index($full_permalink)
  {
    $article = Article::where('full_permalink', '=', $full_permalink)->firstOrFail();

    $data = [
      'article' => $article,
    ];

    return \View::make('blog::article.images', $data);
  }

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store($full_permalink)
	{
    $article = Article::where('full_permalink', '=', $full_permalink)->firstOrFail();

    $data = \Input::all();
    $data['blog_article_id'] = $article->id;
    $data['featured'] = (\Input::has('featured')) ? true : false;
    $data['active'] = (\Input::has('active')) ? true : false;

    $validator = \Validator::make($data, $this->rules);

    if($validator->fails()) {
      return \Redirect::back()->withErrors($validator)->withInput();
    }

    if($image = ArticleImage::create($data)) {

      $data = [
        'article' => $article,
        'image' => $image
      ];

      return \View::make('blog::partials.image', $data);
    }
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */

	public function update($full_permalink, $id)
	{
    $article = Article::where('full_permalink', '=', $full_permalink)->firstOrFail();

    $image= ArticleImage::findOrFail($id);

    $data = \Input::all();
    $data['blog_article_id'] = $article->id;
    $data['featured'] = (\Input::has('featured')) ? true : false;
    $data['active'] = (\Input::has('active')) ? true : false;

    $validator = \Validator::make($data, $this->rules);

    if($validator->fails()) {
      return \Redirect::back()->withErrors($validator)->withInput();
    }

    if($image->update($data)) {

      $data = [
        'article' => $article,
        'image' => $image
      ];

      return \View::make('blog::partials.image', $data);
    }
	}


  public function delete($full_permalink, $id) {
    $image = ArticleImage::findOrFail($id);

    if($image->delete()) {
      $images = ArticleImage::where('blog_article_id', '=', $image->blog_article_id)->orderBy('order', 'asc')->orderBy('updated_at', 'desc')->get();
      \Session::flash('message', 'Article Image successfully deleted!');
      return \Redirect::back();
    }
  }

  public function order($id)
  {
    $image = ArticleImage::findOrFail($id);
    $order = $image->order;
    $image->order = \Input::get('order');
    $image->save();

    if($order > $image->order) {
      $images = ArticleImage::where('blog_article_id', '=', $image->blog_article_id)->orderBy('order', 'asc')->orderBy('updated_at', 'desc')->get();
    } else {
      $images = ArticleImage::where('blog_article_id', '=', $image->blog_article_id)->orderBy('order', 'asc')->orderBy('updated_at', 'asc')->get();
    }

    $i = 1;
    foreach($images as $image) {
      $image->order = $i;
      $i++;
      $image->save();
    }

    \Session::flash('message', 'Article Image order successfully updated!');
    return \Redirect::back();
  }

}
