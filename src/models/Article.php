<?php namespace WorkInProgress\Blog;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Article extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'blog_articles';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  protected $fillable = ['blog_category_id', 'title', 'seo_title', 'permalink', 'full_permalink', 'short_description', 'description', 'url', 'featured', 'active', 'order', 'template_id', 'premium'];

  static function _generateFullPermalink($category, $value, $permalink = array())
  {
    $permalink[] = $value;

    if($category->parentCategory()->first() && $category->parentCategory()->first()->permalink != 'articles') {
      $permalink = Article::_generateFullPermalink($category->parentCategory()->first(), $category->parentCategory()->first()->permalink, $permalink);
    }

    return $permalink;
  }

  static function generateFullPermalink($article, $value)
  {
    $permalink = array();
    $permalink = Article::_generateFullPermalink($article, $value, $permalink);
    $permalink = array_reverse($permalink);

    return implode('/', $permalink);
  }

  public static function boot()
  {
    parent::boot();

    Article::creating(function($article) {
      $article->full_permalink = Article::generateFullPermalink($article, $article->permalink); 
      $article->order = Article::where('blog_category_id', '=', $article->blog_category_id)->count()+1;
    });

    Article::updating(function($article) {
      $article->full_permalink = Article::generateFullPermalink($article, $article->permalink); 
    });

    Article::deleted(function($article) {
      $articles = Article::where('blog_category_id', '=', $article->article_category_id)->orderBy('order', 'asc')->get();

      $i = 1;
      foreach($articles as $article) {
        $article->order = $i;
        $i++;
        $article->save();
      }
    });
  }

  public function parentCategory()
  {
    return $this->belongsTo('\WorkInProgress\Blog\Category', 'blog_category_id');
  }

  public function images()
  {
    return $this->hasMany('\WorkInProgress\Blog\ArticleImage', 'blog_article_id');
  }

  public function getFullTitleAttribute()
  {
    return $this->attributes['seo_title'] ?: $this->attributes['title'];
  }
}

?>
