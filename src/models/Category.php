<?php namespace WorkInProgress\Blog;

use Illuminate\Database\Eloquent\SoftDeletingTrait;

class Category extends \Eloquent {

  use SoftDeletingTrait;

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'blog_categories';

  protected $dates = ['created_at', 'updated_at', 'deleted_at'];

  protected $guarded = ['id'];

  protected $fillable = ['blog_category_id', 'title', 'seo_title', 'permalink', 'full_permalink', 'short_description', 'description', 'active', 'order', 'template_id'];

  static function _generateFullPermalink($category, $value, $permalink = array())
  {
    $permalink[] = $value;

    if($category->parentCategory()->first() && $category->parentCategory()->first()->permalink != 'articles') {
      $permalink = Category::_generateFullPermalink($category->parentCategory()->first(), $category->parentCategory()->first()->permalink, $permalink);
    }

    return $permalink;
  }

  static function generateFullPermalink($category, $value)
  {
    $permalink = array();


    if($value == 'articles') {
      return 'articles';
    }

    $permalink = Category::_generateFullPermalink($category, $value, $permalink);
    $permalink = array_reverse($permalink);

    return implode('/', $permalink);
  }

  public static function boot()
  {
    parent::boot();

    Category::creating(function($category) {
      $category->full_permalink = Category::generateFullPermalink($category, $category->permalink); 
      $category->order = Category::where('blog_category_id', '=', $category->blog_category_id)->count()+1;
    });

    Category::updating(function($category) {
      $category->full_permalink = Category::generateFullPermalink($category, $category->permalink); 

      if(count($category->categories)) {
        foreach($category->categories as $category) {
          $category->full_permalink = Category::generateFullPermalink($category, $category->permalink); 
          $category->save();
        }
      }

      if(count($category->articles)) {
        foreach($category->articles as $article) {
          $article->full_permalink = Article::generateFullPermalink($article, $article->permalink); 
          $article->save();
        }
      }
    });

    Category::deleted(function($category) {
      $categories = Category::where('blog_category_id', '=', $category->blog_category_id)->orderBy('order', 'asc')->get();

      $i = 1;
      foreach($categories as $category) {
        $category->order = $i;
        $i++;
        $category->save();
      }

      $articles = Article::where('blog_category_id', '=', $category->blog_category_id)->orderBy('order', 'asc')->get();

      $i = 1;
      foreach($articles as $article) {
        $article->order = $i;
        $i++;
        $article->save();
      }
    });
  }

  public function articles()
  {
    return $this->hasMany('\WorkInProgress\Blog\Article', 'blog_category_id');
  }

  public function categories()
  {
    return $this->hasMany('\WorkInProgress\Blog\Category', 'blog_category_id');
  }

  public function parentCategory()
  {
    return $this->belongsTo('\WorkInProgress\Blog\Category', 'blog_category_id');
  }

  public function images()
  {
    return $this->hasMany('\WorkInProgress\Blog\CategoryImage', 'blog_category_id');
  }

  public function getFullTitleAttribute()
  {
    return $this->attributes['seo_title'] ?: $this->attributes['title'];
  }

}

?>
