<?php namespace WorkInProgress\Blog;

class CategoryImage extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'blog_category_images';

  protected $dates = ['created_at', 'updated_at'];

  protected $guarded = array('id');

  public function parentCategory()
  {
    return $this->belongsTo('\WorkInProgress\Blog\Category', 'blog_category_id');
  }

}

?>
