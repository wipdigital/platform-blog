<?php namespace WorkInProgress\Blog;

class ArticleImage extends \Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'blog_article_images';

  protected $dates = ['created_at', 'updated_at'];

  protected $guarded = array('id');

  public static function boot()
  {
    parent::boot();

    ArticleImage::creating(function($image) {
      $image->order = ArticleImage::where('blog_article_id', '=', $image->blog_article_id)->count()+1;
    });

    ArticleImage::deleted(function($image) {
      $images = ArticleImage::where('blog_article_id', '=', $image->blog_article_id)->orderBy('order', 'asc')->get();

      $i = 1;
      foreach($images as $image) {
        $image->order = $i;
        $i++;
        $image->save();
      }
    });
  }

  public function parentArticle()
  {
    return $this->belongsTo('\WorkInProgress\Blog\Article', 'blog_article_id');
  }

}

?>
